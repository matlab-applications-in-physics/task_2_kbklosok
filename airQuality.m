%MATLAB 2020b
%name: airQuailty
%author: kbklosok
%date: 16.11.2020
%version: 1.3
clear;clc;

file_id = fopen('result.txt', 'w');

exceeded_hours = "";
exceeded_days = "";

%Defining acceptable levels = [SO2,NO2,NOx,NO,O3, O3-8h, CO, CO-8h, PM10]
acceptable_levels = [350, 200, 0, 0, 0, 120, 0, 10000, 0];
acceptable_levels_24h = [125,0,0,0,0,0,0,0,50];

file_list = dir('air_quality_data_Zabrze');
file_index = 3; %file iterator, first file index = 3

times = [];
values = [];
dates = [];

%Loop through all files
while file_index<=length(file_list)
    
    file_name = file_list(file_index).name;
    folder = 'air_quality_data_Zabrze\';
    file = strcat(folder, file_name); %merging file_name and folder to create file id
    [filepath, name, ext] = fileparts(file); %dividing file_name from extension 
    file_date = replace(name, 'dane-pomiarowe_', '');%date is given in file_name
    
    %Converting file data
    string_table = readlines(file); %Grouping into rows
    string_table = split(string_table,';'); %Spliting columns
    string_table = replace(string_table,',','.'); %Replacing ',' for easier values converting
    string_table = replace(string_table,"""",''); %Removing unnecessary quotation marks
    
    %Dividing main table into more specific ones
    headlines = string_table(1,:);
    time_table = string_table(2:25,1);
    value_table = double(string_table(2:25,2:10));%Converting values into doubles
    
    % Creating 24x1 array containing file date
    date_table = zeros(24,1); %Alocating space
    date_table = string(date_table);%Converting double array to string array
    date_table(:) = file_date;
    
    %Creating arrays containing informations from all files
    times = cat(1,times,time_table);
    values = cat(1,values, value_table);
    dates = cat(1,dates,date_table);
    
    file_index =file_index + 1;
end


fprintf(file_id, 'Przekroczone wartości: \n');

%Iterating through acceptable values
for i = 1:length(acceptable_levels)
    %Checking if 1 hour\8 hour average values fit in standards
    if acceptable_levels(i) ~= 0
        for j = 1:length(times)
            if values(j,i)>acceptable_levels(i)
                fprintf(file_id2, 'Wartość przekroczona: %s %s \t %s \t %.1f \n',dates(j),...
                    times(j), headlines(i+1), values(j,i));
                
                exceeded_days = exceeded_days + dates(j) + ";";
                exceeded_hours = exceeded_hours + dates(j) + " " + times(j) + ";";
            end
        end
    end
    
    %Checking if 24 hour average fit in standards
    if acceptable_levels_24h(i) ~= 0 
        for k = 1:length(times)-23
            if sum(values(k:k+23,i))/24 > acceptable_levels_24h(i)
                fprintf(file_id, 'Średnia 24h przekroczona: %s %+5s - %s %+5s \t %s \t %.1f \t Poz. Dop. %.1f \n',...
                        dates(k), times(k),dates(k+23),times(k+23),headlines(i+1),...
                        sum(values(k:k+23,i))/24,acceptable_levels_24h(i) );
                
                exceeded_days = exceeded_days + dates(k) + ";";
                exceeded_hours = exceeded_hours + dates(k) +" "+ times(k) + " - " + dates(k+23)+" " + times(k+23)+ ";";
            end
        end
    end
end

exceeded_days = split(exceeded_days, ';');
exceeded_days = unique(exceeded_days); % method 'unique' also counts whitespace
exceeded_days(1) = []; %removing unwanted whitespace

exceeded_hours = split(exceeded_hours, ';');
exceeded_hours = unique(exceeded_hours); 
exceeded_hours(1) = []; 

amount_of_days = length(file_list)-2; %file_list contains '.' and '..'
fprintf(file_id, "\nIlośc dni branych pod uwagę: %d\n", amount_of_days); 
fprintf(file_id, "Ilość dni w których wystąpiły przekroczenia wartości dopuszczalnych: %d \n",length(exceeded_days));
fprintf(file_id, "Ile średnio dni w tygodniu: %.0f\n",length(exceeded_days)/(amount_of_days/7));
fprintf(file_id, "Ile razy doszło do przekroczenia wartości dopuszczalnych: %d (brane pod uwagę są również przedziały 24 godzinne)\n",length(exceeded_hours));
fprintf(file_id, "Ile godzin dziennie: %.0f (brane pod uwagę są również przedziały 24 godzinne)\n",length(exceeded_hours)/(amount_of_days));
fprintf(file_id, "Komentarz:\nBrak w pomiarach(wynikający prawdopowobnie z awarii sprzętu) może zaniżać ilość\nprzekroczeń wartości dopuszczalnych - przy obliczeniach brak wartości przyjmowany był jako 0.");

%Displaying results
result_file = fopen(file_id);
text = readlines(result_file);
disp(text)

fclose(file_id);

